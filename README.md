# Open Epicuri Projects #

This repository contains all open source projects that integrate with the [Epicuri M/EPOS System](http://www.epicuri.co.uk).

**NB All projects require the recipient to have a subscription to an Epicuri account. Please contact [info@epicuri.co.uk](mailto:info@epicuri.co.uk) for more information ** 

*We offer up these freely usable projects in good faith and hope that you will not abuse our systems to the detriment of everyone else. Be cool.*